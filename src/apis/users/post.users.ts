
type Request = UsersApi.Post.Request;
type Response = UsersApi.Post.Response;

export const post: Handler.Callback<Request, Response> = async(req, res) => {
  await new Promise(resolve => setTimeout(resolve, 2000));

  res.status(200).json({
    accessToken: Date.now().toString(),
    email: req.body.email,
    firstName: req.body.firstName,
    lastName: req.body.lastName,
  });
};
