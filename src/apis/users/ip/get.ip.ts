// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import {HttpClient} from '~/clients';

type Request = UsersApi.Ip.Get.Request;
type Response = UsersApi.Ip.Get.Response;

const {NEXT_PUBLIC_IP_API_URL} = process.env;

const httpClient = new HttpClient({
  baseUrl: NEXT_PUBLIC_IP_API_URL,
});

export const get: Handler.Callback<Request, Response> = async(req, res) => {
  const response = await httpClient.get<UsersApi.Ip.Get.Result>({
    path: '/json',
    config: {
      params: {
        fields: [
          'query',
          'country',
          'city',
          'region',
          'lat',
          'lon',
        ],
      },
    },
  });

  res.status(200).json({
    city: response.data.city,
    country: response.data.country,
    ip: response.data.query,
    state: response.data.region,
    geolocation: {
      latitude: response.data.lat,
      longitude: response.data.lon,
    },
  });
};
