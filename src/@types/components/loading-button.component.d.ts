
declare namespace LoadingButton {
  interface Props {
    className?: string;
    disabled?: boolean;
    isLoading: boolean;
    onClick: MouseEventHandler;
  }
}
