
declare namespace HttpClient {
  type RequestParams = {
    path: string;
    config?: ExternalModules.Axios.AxiosRequestConfig;
  };
}
