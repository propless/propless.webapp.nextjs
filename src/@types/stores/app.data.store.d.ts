
declare namespace AppStore {
  interface State {
    clicks: number;
    mousePosition: {
      x: number;
      y: number;
    };
  }
}
