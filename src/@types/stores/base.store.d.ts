
declare namespace BaseStore {
  type Config = {
    persistence?: {
      storageKey: string;
    };
  };

  type Reaction<TState> = {
    path: Array<string>,
    callback: (
      value: ExternalModules.Hookstate.State<TState>,
      data: ExternalModules.Hookstate.PluginCallbacksOnSetArgument,
    ) => void,
  };
}
