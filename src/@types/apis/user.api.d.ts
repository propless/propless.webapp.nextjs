
declare namespace UsersApi {

  namespace Post {
    interface Request extends ExternalModules.Next.NextApiRequest {
      body: {
        email: string;
        firstName: string;
        lastName: string;
        password: string;
      }
    }

    type Result = { }

    type Response = {
      email: string;
      firstName: string;
      lastName: string;
      accessToken: string;
    }
  }

  namespace Ip {
    namespace Get {

      type Request = ExternalModules.Next.NextApiRequest;

      type Result = {
        query: string;
        country: string;
        region: string;
        city: string;
        lat: number;
        lon: number;
      }

      type Response = {
        ip: string;
        country: string;
        state: string;
        city: string;
        geolocation: {
          latitude: number;
          longitude: number;
        }
      }
    }
  }

}
