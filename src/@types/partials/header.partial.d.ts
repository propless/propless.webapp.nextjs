
declare namespace Header {
  namespace Brand {
    interface Props {
      onClick: () => void;
    }
  }
}
