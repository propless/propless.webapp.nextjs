
declare namespace HomeLayout {

  type Props = Partial<ServerSideProps>;

  type ServerSideProps = UserApi.Ip.Handler.Response;

  namespace Client {
    type HelloDto = HelloApi.Handler.Response;
  }

  namespace Controller {
    type State = {
      isLoading: boolean;
    }
  }
}
