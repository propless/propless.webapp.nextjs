
declare namespace LoginLayout {
  namespace Form {
    interface Values {
      email: string;
      firstName: string;
      lastName: string;
      password: string;
    }
  }
}
