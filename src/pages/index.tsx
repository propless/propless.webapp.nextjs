
import {default as HomePage} from './home';
import {getServerSideProps as homeGetServerSideProps} from './home';

export default HomePage;
export const getServerSideProps = homeGetServerSideProps;

