
import {default as HomeLayout} from '~/layouts/home/home.layout';
import {getServerSideProps as homeGetServerSideProps} from '~/layouts/home/home.layout';

export default HomeLayout;
export const getServerSideProps = homeGetServerSideProps;
