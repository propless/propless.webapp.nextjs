
import NextDocument, {DocumentContext, DocumentInitialProps} from 'next/document';
import {ServerStyleSheet} from 'styled-components';

export default class Document extends NextDocument {

  public static async getInitialProps(
    context: DocumentContext
  ): Promise<DocumentInitialProps> {

    const sheet = new ServerStyleSheet();
    const originalRenderPage = context.renderPage;

    try {
      context.renderPage = () =>
        originalRenderPage({
          enhanceApp: (App) => (props) =>
            sheet.collectStyles(<App {...props} />),
        });
      const initialProps = await NextDocument.getInitialProps(context);
      return {
        ...initialProps,
        styles: (
          <>
            {initialProps.styles}
            {sheet.getStyleElement()}
          </>
        ),
      };
    } finally {
      sheet.seal();
    }
  }

}
