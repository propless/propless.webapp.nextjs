import {handler} from '~/apis/handler';
import {get} from '~/apis/users/ip/get.ip';

export default handler({get});
