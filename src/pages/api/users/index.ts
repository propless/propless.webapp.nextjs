
import {handler} from '~/apis/handler';
import {post} from '~/apis/users/post.users';

export default handler({post});
