import {css} from 'styled-components';

export const spin = ({
  animationName,
  duration = 1,
  iterations = 'infinite',
  timingFunction = 'linear',
}: {
  animationName: string;
  duration?: number;
  iterations?: 'infinite' | number;
  timingFunction?: 'linear' | 'ease'; // Add as you please.
}) => css`
  @keyframes ${animationName} {
    from {
      transform: rotate(0deg);
    }
    to {
      transform: rotate(360deg);
    }
  }

  animation-duration: ${duration}s;
  animation-iteration-count: ${iterations};
  animation-name: ${animationName};
  animation-timing-function: ${timingFunction};
`;
