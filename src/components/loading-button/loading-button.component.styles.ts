
import {Spinner2} from '@styled-icons/icomoon/Spinner2';
import styled, {css} from 'styled-components';

import {BaseLabel, PrimaryButton} from '~/atoms';
import {spin} from '~/styles';

export const Wrapper = styled(PrimaryButton)`
  display: flex;
`;

export const Label = styled(BaseLabel)<{ extraMargin: boolean; }>`
  align-self: center;
  font-size: 12px;
  font-weight: bold;
  transition: ease .3s;

  ${({theme}) => css`
    color: ${theme.colors.primary.contrast};
  `}

  ${({extraMargin, theme}) => extraMargin && css`
    margin-right: 10px;
  `}
`;

export const Loading = styled(Spinner2)<{ show: boolean; }>`
  ${spin({
 animationName: 'loading-button-spin',
})}
  align-self: center;
  height: 16px;
  opacity: 0;
  transform: scale(0);
  transition: ease .3s;
  width: 0;

  ${({show}) => show && css`
    opacity: 1;
    transform: scale(1);
    width: 16px;
  `}
`;
