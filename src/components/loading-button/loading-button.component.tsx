
import {FC} from 'react';

import {
 Label, Loading, Wrapper,
} from './loading-button.component.styles';

const LoadingButton: FC<LoadingButton.Props> = ({
  className,
  children,
  onClick,
  isLoading,
  disabled,
}) => (
  <Wrapper
    className={className}
    onClick={onClick}
    disabled={disabled || isLoading}
  >
    <Label extraMargin={isLoading}>
      {children}
    </Label>
    <Loading show={isLoading} />
  </Wrapper>
);

export default LoadingButton;
