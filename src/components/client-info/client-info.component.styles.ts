
import styled from 'styled-components';

import {BaseLabel} from '~/atoms';

export const Wrapper = styled.div`
  bottom: 20px;
  left: 20px;
  position: fixed;
`;

export const ItemWrapper = styled.div`
  display: flex;
  margin-bottom: 5px;
`;

export const Indicator = styled(BaseLabel)`
  align-self: center;
  font-size: 12px;
  font-weight: bold;
  margin-right: 5px;
  text-transform: uppercase;
`;

export const Value = styled(Indicator)`
  align-self: center;
  font-weight: normal;
  margin: 0;
`;
