
import {FC, useMemo} from 'react';
import {useHookstate} from '@hookstate/core';
import {AppStore, UserDataStore} from '~/stores';
import {
  Indicator, ItemWrapper, Value, Wrapper,
} from './client-info.component.styles';

const IpInfo: FC = () => {

  const {mousePosition, clicks} = useHookstate(AppStore.state);
  const {
    email,
    ip,
    fullName,
    sessionStatus,
  } = useHookstate(UserDataStore.state);

  const items: Array<{
    indicator: string;
    value: string;
  }> = useMemo(() => {
    return [ ...fullName.value && email.value ? [{
      indicator: 'email',
      value: email.value,
    }, {
      indicator: 'name:',
      value: fullName.value,
    }] : [], {
      indicator: 'cursor:',
      value: `${mousePosition.x.value} x ${mousePosition.y.value}`,
    }, {
      indicator: 'clicks:',
      value: clicks.value.toString(),
    }, ...sessionStatus.value && ip.value ? [{
      indicator: 'client ip:',
      value: ip.value,
    }] : [] ];
  }, [
    email.value,
    clicks.value,
    fullName.value,
    ip.value,
    mousePosition.x.value,
    mousePosition.y.value,
    sessionStatus.value,
  ]);

  return (
    <Wrapper>
      {items.map(({indicator, value}) => (
        <ItemWrapper key={indicator}>
          <Indicator>
            {indicator}
          </Indicator>
          <Value>
            {value}
          </Value>
        </ItemWrapper>
      ))}
    </Wrapper>
  );
};

export default IpInfo;
