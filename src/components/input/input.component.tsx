
import {FC} from 'react';

import {
 ErrorMessage, Label, TextInput, Wrapper,
} from './input.component.styles';

const Input: FC<InputComponent.Props> = ({
  className,
  errorMessage,
  id,
  label,
  name,
  onBlur,
  onChange,
  onFocus,
  placeholder,
  type,
  value,
}) => (
  <Wrapper className={className}>
    {label && (
      <Label htmlFor={id}>
        {label}
      </Label>
    )}
    <TextInput
      id={id}
      name={name}
      onBlur={onBlur}
      onChange={onChange}
      onFocus={onFocus}
      placeholder={placeholder}
      type={type}
      value={value}
    />
    <ErrorMessage>
      {errorMessage}
    </ErrorMessage>
  </Wrapper>
);

export default Input;
