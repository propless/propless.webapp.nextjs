
import styled from 'styled-components';

const {NEXT_PUBLIC_BASE_URL} = process.env;

export const Logo = styled.img.attrs({
  src: `${NEXT_PUBLIC_BASE_URL}/images/logo256.png`,
})`
  display: block;
  height: 512px;
  width: 512px;
`;
