
import {useEffect, useMemo} from 'react';
import {useHookstate} from '@hookstate/core';
import type {GetServerSideProps, NextPage} from 'next';
import {HttpClient} from '~/clients';
import {ClientInfo} from '~/components';
import {UserDataStore} from '~/stores';
import Button from './button/button';
import {
Label, Title, Wrapper,
} from './home.layout.styles';

type ServerSideProps = HomeLayout.ServerSideProps;

const HomeLayout: NextPage<HomeLayout.Props> = (props) => {

  // TODO: Look for a better approach.
  useEffect(() => {
    UserDataStore.state.location.set({
      city: props.city,
      country: props.country,
      latitude: props.geolocation.latitude,
      longitude: props.geolocation.longitude,
      state: props.state,
    });
    UserDataStore.state.ip.set(props.ip);
  }, [
    props.city,
    props.country,
    props.geolocation.latitude,
    props.geolocation.longitude,
    props.ip,
    props.state,
  ]);

  const {ip, location} = useHookstate(UserDataStore.state);

  const mainLabel = useMemo<string>(() => {
    let label: string;
    if(ip.value) {
      label = `Your IP is ${ip.value} and you're `;
      label += `at ${location.city.value} - ${location.state.value}.`;
    } else {
      label = 'Find out your IP clicking down bellow.';
    }

    return label;
  }, [ ip.value, location.city.value, location.state.value ]);

  return (
    <Wrapper>
      <Title>
        IP Finder!
      </Title>
      <Label>
        {mainLabel}
      </Label>
      <Button />
      <ClientInfo />
    </Wrapper>
  );
};

export const getServerSideProps: GetServerSideProps<ServerSideProps> = async() => {
  const httpClient = new HttpClient();

  const ip = await httpClient.get<UsersApi.Ip.Get.Response>({
    path: '/api/users/ip',
  });

  return {
    props: ip.data,
  };
};

export default HomeLayout;
