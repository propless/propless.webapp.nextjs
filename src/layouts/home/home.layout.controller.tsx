
import {BaseStore, UserDataStore} from '~/stores';

export class Controller extends BaseStore<HomeLayout.Controller.State> {

  constructor() {
    super({
      isLoading: false,
    });
  }

  public async getIp() {
    this.state.isLoading.set(true);

    setTimeout(async() => {
      try {
        await UserDataStore.getIp();
      } finally {
        this.state.isLoading.set(false);
      }
    }, 1000);
  }

}

export const HomePageController = new Controller();
