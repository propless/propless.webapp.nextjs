
import styled from 'styled-components';

import {BaseLabel, PageTitle} from '~/atoms';

export const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
`;

export const Title = styled(PageTitle)`
  align-self: center;
`;

export const Label = styled(BaseLabel)`
  align-self: center;
  margin-bottom: 50px;
`;
