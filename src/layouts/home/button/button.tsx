
import {FC, useCallback} from 'react';
import {useHookstate} from '@hookstate/core';
import {UserDataStore} from '~/stores';
import {HomePageController} from '../home.layout.controller';
import {Wrapper} from './button.styles';

const Button: FC = () => {
  const {ip} = useHookstate(UserDataStore.state);
  const {isLoading} = useHookstate(HomePageController.state);

  const handleClick = useCallback(async() => {
    await HomePageController.getIp();
  }, [ ]);

  return (
    <Wrapper
      onClick={handleClick}
      isLoading={isLoading.get()}
    >
      {(!ip.get() && !isLoading.get()) && 'find my ip'}
      {isLoading.get() && 'loading'}
      {(!!ip.get() && !isLoading.get()) && 'update'}
    </Wrapper>
  );
};

export default Button;
