
import {NextPage} from 'next';
import {ClientInfo} from '~/components';
import Form from './form/form';
import {Wrapper} from './login.layout.styles';

const LoginView: NextPage = () => (
  <Wrapper>
    <Form />
    <ClientInfo />
  </Wrapper>
);

export default LoginView;
