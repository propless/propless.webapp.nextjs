
import styled from 'styled-components';
import {
BaseLabel, PageTitle, SecondaryButton,
} from '~/atoms';

export const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
`;

export const Title = styled(PageTitle)`
  align-self: center;
`;

export const Paragraph = styled(BaseLabel).attrs({
  as: 'p',
})`
  max-width: 600px;
  text-align: center;
  align-self: center;
  margin-bottom: 10px;
`;

export const ParagraphLink = styled.a.attrs({
  target: '_blank',
})``;

export const DocsButton = styled(SecondaryButton)`
  align-self: center;
  margin-top: 50px;

  &:hover {
    transform: scale(1.1);
  }
`;
