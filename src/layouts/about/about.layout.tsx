
import {NextPage} from 'next';
import {ClientInfo} from '~/components';

import {
 DocsButton, Paragraph, ParagraphLink, Title, Wrapper,
} from './about.layout.styles';

const {NEXT_PUBLIC_APP_DOCS_URL} = process.env;

const AboutView: NextPage = () => {

  const handleDocsButtonClick = () => {
    window.open(NEXT_PUBLIC_APP_DOCS_URL, '_blank');
  };

  return (
    <Wrapper>
      <Title>
        About!
      </Title>
      <Paragraph>
        This project was created to materialize the concepts expressed by the Propless Architecture.
      </Paragraph>
      <Paragraph>
        For more information access the&nbsp;
        <ParagraphLink href={NEXT_PUBLIC_APP_DOCS_URL}>
          react template architecture docs
        </ParagraphLink>
        {'. There you\'ll find layer descriptions, diagrams and everything you need to know about the tópic.'}
      </Paragraph>
      <Paragraph>
        If you notice any problems or want to contribute somehow, do not hesitate. Just make sure to have read and understood the architectural docs and send the project a pull-request!
      </Paragraph>
      <DocsButton onClick={handleDocsButtonClick}>
        see the docs
      </DocsButton>
      <ClientInfo />
    </Wrapper>
  );
};

export default AboutView;
